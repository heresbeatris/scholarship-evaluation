﻿using ApiExam.Models;
using ApiExam.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApiExam.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EmployeesController : ControllerBase
    {

        IEmployeeCollectionService _employeeCollectionService;
        public EmployeesController(IEmployeeCollectionService employeeCollectionService)
        {
            _employeeCollectionService = employeeCollectionService;
        }


        [HttpGet]
        public async Task<IActionResult> GetEmployees()
        {

            List<Employee> employees = await _employeeCollectionService.GetAll();
            return Ok(employees);
        }

        [HttpPost]
        public async Task<IActionResult> CreateEmployees([FromBody] Employee employee)
        {
            if (employee == null)
            {
                return BadRequest();
            }


            if (await _employeeCollectionService.Create(employee))
            {
                return CreatedAtRoute("GetByEmployeeId", new { id = employee.Id.ToString() }, employee);
            }
            return RedirectToAction("GetByEmployeeId");

        }

        [HttpGet("{idEmployee}", Name = "GetByEmployeeId")]
        public async Task<IActionResult> GetByEmployeeId(Guid idEmployee)
        {
            if (idEmployee == null)
            {
                return BadRequest();
            }
            var employee = await _employeeCollectionService.Get(idEmployee);

            if (employee == null)
            {
                return NotFound();
            }
            return Ok(employee);
        }

        // <summary>
        // 
        // </summary>
        // <response code = "400" > Bad request</response>
        // <response code = "404" > Not found</response>
        // <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateEmployee(Guid id, [FromBody] Employee employeeToUpdate)
        {
            if (employeeToUpdate == null)
            {
                return BadRequest();
            }

            if (await _employeeCollectionService.Update(id, employeeToUpdate))
            {
                return NoContent();
            }

            return BadRequest("Employee update failed");
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteEmployee(Guid id)
        {
            if (id == null)
            {
                return BadRequest();
            }
            bool removed = await _employeeCollectionService.Delete(id);

            if (removed)
            {
                return NoContent();
            }

            return NotFound();
        }


    }
}
