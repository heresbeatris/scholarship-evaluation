﻿using ApiExam.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiExam.Services
{
    public interface IEmployeeCollectionService : ICollectionService<Employee>
    {
        
    }
}
