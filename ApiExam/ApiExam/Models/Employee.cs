﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ApiExam.Models
{
    public class Employee
    {
        public Guid Id { get; set; }
        public string Salary { get; set; }
        [Required]
        public string Name { get; set; }
        public string Function { get; set; }
        public string CategoryId { get; set; }
        public string DateOfBirth { get; set; }
    }
}
