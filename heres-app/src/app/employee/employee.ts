export interface Employee {
    id?: string;
    name: string;
    dateOfBirth: string;
    function: string;
    salary: string;
    categoryId: string;
}
