import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmployeeService } from '../services/employee.service';
import { Employee } from './employee';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements OnInit {

  employees: Employee[] = [];
  @Input() selectedCategoryId: string;
  constructor(private employeeService: EmployeeService, private router: Router) { }

  ngOnInit(): void {
    return this.getEmployee();
  }

  ngOnChanges(): void {
    if (this.selectedCategoryId) {
      this.employeeService.getFiltredEmployees(this.selectedCategoryId).subscribe((result) =>{
        this.employees  = result;
        });
    }

  }
  
  getEmployee(){
    this.employeeService.getEmployee().subscribe((result) =>{
      this.employees  = result;
      });
    }
   
    deleteEmployee(id: string) {
      console.log(id);
      
      this.employeeService.deleteEmployee(id).subscribe(() =>this.getEmployee());
      
     // this.ngOnInit();
    }
  
    getEmployeeById() {
      const inputElement = <HTMLInputElement>document.getElementById("searchInput");
      let id: string = " ";
      id = inputElement.value;
      if (id !== " ")
        this.employeeService.getEmployeeById(id).subscribe((result) => this.employees = result);
      else
        this.ngOnInit();
    }
    
    getEmployeeByName() {
      const inputElement = <HTMLInputElement>document.getElementById("searchInputDescription");
      let name: string = " ";
      name = inputElement.value;
      console.log(name);
      if (name !== "")
        this.employeeService.getEmployeeByName(name).subscribe((result) => this.employees = result);
      else
        this.ngOnInit();
    }
  
    getNoteByDateOfBirth() {
      const inputElement = <HTMLInputElement>document.getElementById("searchInputTitle");
      let dateOfBirth: string = "";
      dateOfBirth = inputElement.value;
      console.log(dateOfBirth);
      if (dateOfBirth !== "")
         this.employeeService.getNoteByDateOfBirth(dateOfBirth).subscribe((result) => this.employees = result);
      else
        this.ngOnInit();
    }
  
     redirectToEdit(id: string) {
       this.router.navigateByUrl('/app-edit-employee/' + id);
    }
  
    
  }

