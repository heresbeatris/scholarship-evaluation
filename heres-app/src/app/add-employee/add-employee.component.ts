import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Category } from '../module/category';
import { EmployeeService } from '../services/employee.service';
import { FilterService } from '../services/filter.service';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.scss']
})
export class AddEmployeeComponent implements OnInit {

  name: string;
  dateOfBirth: string;
  categories: Category[];
  idCategoryEmployee: string;
  function: string;
  salary: string;
  constructor(private filterService: FilterService, private employeeService: EmployeeService, private router: Router) { }

  ngOnInit(): void {
    this.categories=this.filterService.getFilters()
  }

  add() {
    this.employeeService.addEmployee(this.name, this.dateOfBirth, this.idCategoryEmployee, this.function, this.salary).subscribe(() => this.router.navigateByUrl(''));
  }
}
