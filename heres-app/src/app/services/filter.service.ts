import { Injectable } from '@angular/core';
import { Category } from '../module/category';

@Injectable({
  providedIn: 'root'
})
export class FilterService {

  categories: Category[] = [
    { name: 'Employeed', id: '1' },
    { name: 'Resigned', id: '2' },
    { name: 'Retired', id: '3' },
  ];

  constructor() { }

  getFilters(): Category[] {
    return this.categories;
  }
}
