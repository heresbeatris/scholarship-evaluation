import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Employee } from '../employee/employee';
import { map } from "rxjs/operators";

@Injectable()
export class EmployeeService {
  
  readonly  baseUrl= "https://localhost:5001";
  readonly httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
    })
  };

  constructor(private HttpClient: HttpClient) { }

  serviceCall() {
    console.log("Note service was called");
  }
  
  getEmployee(): Observable<Employee[]> {
    return this.HttpClient.get<Employee[]>(this.baseUrl + '/employees', this.httpOptions);
  }

  getFiltredEmployees(categoryId: string): Observable<Employee[]>{
    return this.HttpClient.get<Employee[]>(this.baseUrl + '/employees', this.httpOptions).pipe(map((employee) => employee.filter((employee) => employee.categoryId === categoryId)));
  }

  getEmployeeById(id: string): Observable<Employee[]>{
    
    return this.HttpClient.get<Employee[]>(this.baseUrl +'/employees/'+ id, this.httpOptions).pipe(map((employee) => employee.filter(employee => employee.id == id)));
  }
  getEmployeeByName(name: string): Observable<Employee[]> {
    return this.HttpClient.get<Employee[]>(this.baseUrl + '/employees', this.httpOptions).pipe(map((employee) => employee.filter(employee => employee.name.toLowerCase() == name)));
  }

  getNoteByDateOfBirth(dateOfBirth: string): Observable<Employee[]> {
    
    return this.HttpClient.get<Employee[]>(this.baseUrl + '/employee', this.httpOptions).pipe(map((employee) => employee.filter(employee => employee.dateOfBirth.toLowerCase() == dateOfBirth)));
  }

  addEmployee(employeeName: string, employeeDateOfBirth: string, employeeFunction: string, salary: string, employeeCategoryId: string ): Observable<Employee> {
   const employee: Employee = {
      
   name: employeeName,
   dateOfBirth:employeeDateOfBirth,
   function: employeeFunction,
   salary: salary,
   categoryId: employeeCategoryId
   }
    return this.HttpClient.post<Employee>(this.baseUrl + '/employees', employee);
   }

  updateEmployee(employee: Employee): Observable<Employee>  {
    return this.HttpClient.put<Employee>(this.baseUrl + '/employees/' + employee.id, employee);
  }

  deleteEmployee(id: string)
  {
    return this.HttpClient.delete(this.baseUrl + '/employees/' + id);
  }


}



