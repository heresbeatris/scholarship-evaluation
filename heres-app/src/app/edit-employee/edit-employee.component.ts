import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Employee } from '../employee/employee';
import { Category } from '../module/category';
import { EmployeeService } from '../services/employee.service';
import { FilterService } from '../services/filter.service';

@Component({
  selector: 'app-edit-employee',
  templateUrl: './edit-employee.component.html',
  styleUrls: ['./edit-employee.component.scss']
})
export class EditEmployeeComponent implements OnInit {

  name: string;
  salary: string;
  dateOfBirth: string;
  idCategoryEmployee: string;
  function: string;
  employee: Employee[];
  categories: Category[];
  id: string;
  sub: any;
 

  constructor(private employeeService: EmployeeService, private route: ActivatedRoute, private filterService:FilterService, private router:Router) { }

  ngOnInit(): void {
    this.sub = this.route.params.subscribe(params => {
      this.id = params['id'];
      console.log(this.id);
      this.employeeService.getEmployeeById(this.id).subscribe((result) => this.employee = result);
    });
    this.categories=this.filterService.getFilters();
  }


  updateEmployee(employee: Employee)  {

    employee.categoryId = this.idCategoryEmployee;
    this.employeeService.updateEmployee(employee).subscribe(() => this.router.navigateByUrl(''));
  }

}
